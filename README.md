#### Project Dependencies:
- python 3
  - pip
  - jupyter
- R 3.3.2

#### Setup project:
- Install the dependencies mentioned in the previous sub-section.
- Run the set-up script located in the root of the project `Rscript setup.R`

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction \n",
    "\n",
    "## Data description\n",
    "The data set is based on one-year vehicle insurance policies taken out in 2004 or 2005. There are 67856 policies, of which 4624 (6.8%) had at least one claim. The variables contained in the dataset are the following:\n",
    "\n",
    "| Variable      | Range                                                  |\n",
    "|-----------    |----------------------------------------------------    |\n",
    "| veh_value     | Vehicle value, in $10,000s                             |\n",
    "| exposure      | 0-1                                                    |\n",
    "| clm           | Occurrence of claim (0 = no, 1 = yes)                  |\n",
    "| numclaims     | Number of claims                                       |\n",
    "| claimcst0     | Claim amount (0 if no claim)                           |\n",
    "| veh_body      | Vehicle body, coded                                    |\n",
    "| veh_age       | Age of vehicle: 1 (youngest), 2, 3, 4                  |\n",
    "| gender        | gender of driver: M, F                                 |\n",
    "| area          | Driver's area of residence: A, B, C, D, E, F           |\n",
    "| agecat        | Driver's age category: 1 (youngest), 2, 3, 4, 5, 6     |\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "car = read.csv(\"data/car.csv\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Data transformation and cleaning\n",
    "## Column type conversion \n",
    "By default, the data structures are not well defined. Factor columns defined as numeric is the most frequent case. It is created a map containing the column names and their base level, and those variables are converted into factors. The base level should be set as the factor with the most occurrences.\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# List of factor names and their respective base levels\n",
    "factor_info= list(\n",
    "  list(name=\"clm\", base_level=\"0\"),\n",
    "  list(name=\"veh_body\", base_level=\"SEDAN\"),\n",
    "  list(name=\"gender\", base_level=\"M\"),\n",
    "  list(name=\"area\", base_level=\"C\"),\n",
    "  list(name=\"veh_age\", base_level=\"1\"),\n",
    "  list(name=\"agecat\", base_level=\"3\")\n",
    ")\n",
    "\n",
    "# Conversion of columns to factors and especification of their levels\n",
    "for(info in factor_info){\n",
    "  car[info$name] = factor(car[[info$name]])\n",
    "  car[info$name] = relevel(car[[info$name]], ref=info$base_level)\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Convert continuous variables into categorical\n",
    "A way to treat outliers is to transform a continuous variable into categorical. It is important to carefully define the number of categories because it impacts the predictability capacity.\n",
    "\n",
    "#### Code\n",
    "\n",
    "In this case, we create a categorical variable named `bands_veh_value` that contains the vehicle values divided in bands."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "car[\"bands_veh_value\"] <- cut(\n",
    "  car[[\"veh_value\"]],\n",
    "  breaks=c(-Inf, 2.5, 5, 7.5, 10, 12.5, Inf),\n",
    "  labels=c(\"<25\",\"25-50\",\"25-75\", \"75-100\", \"100-125\", \">125\")\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Generalized Linear Models\n",
    "\n",
    "## Logistic model\n",
    "The occurrence of a claim can be modeled using a generalized linear model with a logit link. The response variable is binary where $1 = claim$ and $0 = no claim$. For logistic models, the logit link ensures predictions of $\\pi$ using $x$. The logit link is the canonical link for the Bernoulli and binomial distributions.\n",
    "\n",
    "### Define link function adjusted to exposure\n",
    "\n",
    "Logistic link function does not take into account the exposure of each sample. Thus, a variation of a logit that considers exposure is needed. The class needed to create a new link function is `link-glm` and it contains four methods (1) `linkfun`, (2) `linkinv`, (3) `mu.eta` and (4) `valideta`. The math formulas are the following:\n",
    "\n",
    "- **Link function:**\n",
    "$$\\eta = \\log \\left(\\frac{\\mu}{exposure - \\mu}\\right)$$\n",
    "\n",
    "- **Inverse link function:**\n",
    "$$\\mu = exposure \\times \\frac{\\exp(\\eta)}{1+\\exp(\\eta)}$$\n",
    "\n",
    "- **Derivative of inverse link function:**\n",
    "$$\\frac{\\partial \\mu}{\\partial \\eta} = exposure \\times \\frac{\\exp(\\eta)}{(1+\\exp(\\eta))^2}$$\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "logit_adjusted = function(exposure) {\n",
    "  linkfun = function(mu) {\n",
    "    if(any(exposure - mu <= 0)){\n",
    "      eta = log((mu)/abs(mu-exposure))\n",
    "    } else {\n",
    "      eta = log((mu)/(exposure-mu))\n",
    "    }\n",
    "    eta\n",
    "  }\n",
    "\n",
    "  linkinv = function(eta) {\n",
    "    thresh = -log(.Machine$double.eps)\n",
    "    eta = pmin(thresh, pmax(eta, -thresh))\n",
    "    exposure * (exp(eta) / (1 + exp(eta)))\n",
    "  }\n",
    "\n",
    "  mu.eta = function(eta) {\n",
    "    thresh = -log(.Machine$double.eps)\n",
    "    ((exposure * exp(eta)) / (1 + exp(eta)) ^ 2)[abs(eta) < thresh]\n",
    "  }\n",
    "\n",
    "  valideta = function(eta) TRUE\n",
    "\n",
    "  link = link = \"logit_adjusted\"\n",
    "\n",
    "  structure(list(linkfun=linkfun, linkinv=linkinv, mu.eta=mu.eta,\n",
    "                 valideta=valideta, name=link), class = \"link-glm\")\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Fitting the model\n",
    "\n",
    "The purpose of this exercise is to replicate the exercises contained in [1]. In [1], it is shown that the risk factors driver's age, area, vehicle body type and vehicle value are all significant in separate regressions, and in the fit having all as explanatory variables. Therefore, they are used in the final model.\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\n",
       "Call:\n",
       "glm(formula = clm ~ agecat + area + veh_body + bands_veh_value, \n",
       "    family = binomial(link = logit_adjusted(car$exposure)), data = car)\n",
       "\n",
       "Deviance Residuals: \n",
       "    Min       1Q   Median       3Q      Max  \n",
       "-0.9970  -0.4480  -0.3390  -0.2149   3.9902  \n",
       "\n",
       "Coefficients:\n",
       "                       Estimate Std. Error z value Pr(>|z|)    \n",
       "(Intercept)            -1.74963    0.04875 -35.891  < 2e-16 ***\n",
       "agecat1                 0.28764    0.06264   4.592 4.39e-06 ***\n",
       "agecat2                 0.06435    0.05011   1.284 0.199075    \n",
       "agecat4                -0.03600    0.04772  -0.754 0.450708    \n",
       "agecat5                -0.26500    0.05567  -4.760 1.93e-06 ***\n",
       "agecat6                -0.25500    0.06694  -3.809 0.000139 ***\n",
       "areaA                  -0.03580    0.04519  -0.792 0.428240    \n",
       "areaB                   0.05338    0.04699   1.136 0.255964    \n",
       "areaD                  -0.13815    0.05846  -2.363 0.018125 *  \n",
       "areaE                  -0.06636    0.06501  -1.021 0.307327    \n",
       "areaF                   0.02086    0.07633   0.273 0.784617    \n",
       "veh_bodyBUS             1.13627    0.44921   2.530 0.011422 *  \n",
       "veh_bodyCONVT          -0.37088    0.64132  -0.578 0.563056    \n",
       "veh_bodyCOUPE           0.43332    0.14843   2.919 0.003507 ** \n",
       "veh_bodyHBACK          -0.01240    0.04314  -0.288 0.773709    \n",
       "veh_bodyHDTOP           0.09897    0.10493   0.943 0.345548    \n",
       "veh_bodyMCARA           0.59606    0.32771   1.819 0.068928 .  \n",
       "veh_bodyMIBUS          -0.11119    0.17178  -0.647 0.517448    \n",
       "veh_bodyPANVN           0.01941    0.14484   0.134 0.893375    \n",
       "veh_bodyRDSTR           0.06962    0.80135   0.087 0.930773    \n",
       "veh_bodySTNWG          -0.01913    0.04995  -0.383 0.701781    \n",
       "veh_bodyTRUCK          -0.09668    0.10823  -0.893 0.371722    \n",
       "veh_bodyUTE            -0.24555    0.07599  -3.232 0.001231 ** \n",
       "bands_veh_value25-50    0.21017    0.04936   4.258 2.06e-05 ***\n",
       "bands_veh_value25-75    0.13652    0.12366   1.104 0.269612    \n",
       "bands_veh_value75-100  -0.60664    0.53884  -1.126 0.260239    \n",
       "bands_veh_value100-125 -0.29001    0.77292  -0.375 0.707503    \n",
       "bands_veh_value>125    -0.79721    1.07082  -0.744 0.456582    \n",
       "---\n",
       "Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1\n",
       "\n",
       "(Dispersion parameter for binomial family taken to be 1)\n",
       "\n",
       "    Null deviance: 33767  on 67855  degrees of freedom\n",
       "Residual deviance: 32494  on 67828  degrees of freedom\n",
       "AIC: 32550\n",
       "\n",
       "Number of Fisher Scoring iterations: 4\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "fit = glm(\n",
    "  clm ~ agecat + area + veh_body + bands_veh_value,\n",
    "  family=binomial(link=logit_adjusted(car$exposure)),\n",
    "  data=car\n",
    ")\n",
    "summary(fit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inverse Gaussian model\n",
    "Claim size is a continuous variable that measures the claim amount resulted from an accident. In [1], the author shows that the distribution of claim size for vehicle insurance resembles an Inverse Gaussian. For continuous response variables, the link function can be gamma or inverse Gaussian. The author, after some research, found that inverse Gaussian model fits better than gamma.\n",
    "\n",
    "### Select samples with a claim\n",
    "\n",
    "The inverse Gaussian model is only capable to analyze samples that incurred in at least one claim. For that, it is needed to create a subset of the complete dataset.\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "samples_with_claim = subset(car, numclaims > 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting the model\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\n",
       "Call:\n",
       "glm(formula = claimcst0 ~ agecat + gender + area, family = inverse.gaussian(link = \"log\"), \n",
       "    data = samples_with_claim)\n",
       "\n",
       "Deviance Residuals: \n",
       "      Min         1Q     Median         3Q        Max  \n",
       "-0.066235  -0.043358  -0.021932   0.001744   0.121605  \n",
       "\n",
       "Coefficients:\n",
       "            Estimate Std. Error t value Pr(>|t|)    \n",
       "(Intercept)  7.68300    0.07224 106.352  < 2e-16 ***\n",
       "agecat1      0.25110    0.09950   2.524  0.01164 *  \n",
       "agecat2      0.09266    0.07664   1.209  0.22676    \n",
       "agecat4     -0.00533    0.07125  -0.075  0.94037    \n",
       "agecat5     -0.12129    0.08140  -1.490  0.13626    \n",
       "agecat6     -0.06755    0.09890  -0.683  0.49461    \n",
       "genderF     -0.15283    0.05119  -2.986  0.00285 ** \n",
       "areaA       -0.07289    0.06806  -1.071  0.28425    \n",
       "areaB       -0.10265    0.06976  -1.471  0.14124    \n",
       "areaD       -0.09781    0.08632  -1.133  0.25725    \n",
       "areaE        0.06951    0.10169   0.684  0.49431    \n",
       "areaF        0.28250    0.12885   2.192  0.02840 *  \n",
       "---\n",
       "Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1\n",
       "\n",
       "(Dispersion parameter for inverse.gaussian family taken to be 0.001464282)\n",
       "\n",
       "    Null deviance: 6.4422  on 4623  degrees of freedom\n",
       "Residual deviance: 6.3765  on 4612  degrees of freedom\n",
       "AIC: 77162\n",
       "\n",
       "Number of Fisher Scoring iterations: 11\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "fit = glm(\n",
    "  claimcst0 ~ agecat + gender + area, \n",
    "  family = inverse.gaussian(link=\"log\"), \n",
    "  data=samples_with_claim\n",
    ")\n",
    "\n",
    "summary(fit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Zero-adjusted inverse Gaussian regression\n",
    "The disadvantage of the generalized linear model using inverse Gaussian function is that it only takes into account the variation resulted from samples with at least one claim. If we want to create a model that also considers the samples with no claim, a different approach is needed.\n",
    "\n",
    "### Generalized Additive Models\n",
    "In Generalized Additive Models for Location Scale and Shape (GAMLSS) the exponential family distribution assumption for the response variable (y) is relaxed and replaced by a general distribution family, including highly skew and/or kurtotic continuous and discrete distributions. The systematic part of the model is expanded to allow modeling not only of the mean (or location) but other parameters of the distribution of $y$ as, linear and/or nonlinear, parametric and/or additive nonparametric functions of explanatory variables and/or random effects.\n",
    "\n",
    "The models for claim size seen before are for positive claim sizes (samples with at least a claim). Analysis of claim size traditionally treats the occurrence of a claim and the amount of a claim, given that there is a claim as separate models. The results of these separate models are combined to produce predictions of expected claim sizes.\n",
    "\n",
    "Suppose $y$ is the claim size with $y = 0$ if there is no claim. Then the distribution of claim size is mixed discrete-continuous.\n",
    "\n",
    "$$\n",
    "  f(y) =\n",
    "  \\begin{cases}\n",
    "    1-t \\pi & \\text{if $y=0$} \\\\\n",
    "    t \\pi h(y) & \\text{if $y > 0,$}\n",
    "  \\end{cases}\n",
    "$$\n",
    "\n",
    "where $t$ is exposure. This resulting distribution is called the zero-adjusted inverse Gaussian (ZAIG) distribution.\n",
    "\n",
    "$$E[y] = t \\pi \\mu, \\quad Var[y] = t \\pi \\mu^2 (1-t \\pi + \\mu \\sigma^2).$$\n",
    "\n",
    "Each of $\\pi$, $\\mu$ and $\\sigma$ is made a function of explanatory variables. The explanatory variables can be different across the three parameters and explanatory variables affect the parameters in different ways.\n",
    "\n",
    "There are several R-packages that can be seen as related to the gamlss packages and to its R implementation. The library used here is **gamlss**.\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Loading required package: splines\n",
      "Loading required package: gamlss.data\n",
      "Loading required package: gamlss.dist\n",
      "Loading required package: MASS\n",
      "Loading required package: nlme\n",
      "Loading required package: parallel\n",
      " **********   GAMLSS Version 5.0-2  ********** \n",
      "For more on GAMLSS look at http://www.gamlss.org/\n",
      "Type gamlssNews() to see new features/changes/bug fixes.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "library(\"gamlss\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define link function adjusted to exposure\n",
    "\n",
    "As well as logistic link function, ZAIG distribution is not adjusted to exposure. The class needed to create a new link function is `link-gamlss` and it needs to contain four methods (1) `linkfun`, (2) `linkinv`, (3) `mu.eta` and (4) `valideta`. The math formulas are the following:\n",
    "\n",
    "- **Link function:**\n",
    "$$\\eta = \\log \\left(\\frac{1 - \\mu}{exposure - (1 - \\mu)}\\right)$$\n",
    "\n",
    "- **Inverse link function:**\n",
    "$$\\mu = (1 - exposure) + \\frac{exposure}{1+\\exp(\\eta)}$$\n",
    "\n",
    "- **Derivative of inverse link function:**\n",
    "$$\\frac{\\partial \\mu}{\\partial \\eta} = -\\frac{exposure \\times \\exp(\\eta)}{(1+\\exp(\\eta))^2}$$\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "adjusted_exposure = function(exposure) {\n",
    "  linkfun = function(mu) {\n",
    "    if (any(exposure - mu <= 0)){\n",
    "      eta = log((1 - mu) / abs(exposure - (1 - mu)))\n",
    "    } else {\n",
    "      eta = log((1 - mu) / (exposure - (1 - mu)))\n",
    "    }\n",
    "    eta\n",
    "  }\n",
    "\n",
    "  linkinv = function(eta){\n",
    "    thresh = -log(.Machine$double.eps)\n",
    "    eta = pmin(thresh, pmax(eta, -thresh))\n",
    "    1 - exposure + exposure / (1 + exp(eta))\n",
    "  }\n",
    "\n",
    "  mu.eta = function(eta) {\n",
    "    thresh = -log(.Machine$double.eps)\n",
    "    ((-exposure * exp(eta))/(1 + exp(eta)) ^ 2)[abs(eta) < thresh]\n",
    "  }\n",
    "\n",
    "  valideta = function(eta) TRUE\n",
    "\n",
    "  link = \"adjusted_exposure\"\n",
    "\n",
    "  structure(\n",
    "    list(linkfun=linkfun, linkinv=linkinv, mu.eta=mu.eta, \n",
    "         valideta=valideta, name=link),\n",
    "    class=\"link-gamlss\"\n",
    "  )\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting the model\n",
    "\n",
    "#### Code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "GAMLSS-RS iteration 1: Global Deviance = 109609.8 \n",
      "GAMLSS-RS iteration 2: Global Deviance = 109609.7 \n",
      "GAMLSS-RS iteration 3: Global Deviance = 109609.7 \n",
      "******************************************************************\n",
      "Family:  c(\"ZAIG\", \"Zero adjusted IG\") \n",
      "\n",
      "Call:  gamlss(formula = claimcst0 ~ agecat + area + gender,  \n",
      "    sigma.formula = ~area, nu.formula = ~agecat + area +  \n",
      "        veh_body + bands_veh_value, family = ZAIG(nu.link = adjusted_exposure(exposure = car$exposure)),  \n",
      "    data = car) \n",
      "\n",
      "Fitting method: RS() \n",
      "\n",
      "------------------------------------------------------------------\n",
      "Mu link function:  log\n",
      "Mu Coefficients:\n",
      "             Estimate Std. Error t value Pr(>|t|)    \n",
      "(Intercept)  7.687005   0.074173 103.636  < 2e-16 ***\n",
      "agecat1      0.256467   0.097854   2.621  0.00877 ** \n",
      "agecat2      0.086311   0.074278   1.162  0.24524    \n",
      "agecat4     -0.003264   0.070152  -0.047  0.96289    \n",
      "agecat5     -0.129357   0.079724  -1.623  0.10469    \n",
      "agecat6     -0.070669   0.098019  -0.721  0.47093    \n",
      "areaA       -0.072993   0.067922  -1.075  0.28253    \n",
      "areaB       -0.102933   0.070296  -1.464  0.14312    \n",
      "areaD       -0.097635   0.082030  -1.190  0.23396    \n",
      "areaE        0.067865   0.093240   0.728  0.46671    \n",
      "areaF        0.281410   0.114522   2.457  0.01400 *  \n",
      "genderF     -0.156136   0.050529  -3.090  0.00200 ** \n",
      "---\n",
      "Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1\n",
      "\n",
      "------------------------------------------------------------------\n",
      "Sigma link function:  log\n",
      "Sigma Coefficients:\n",
      "             Estimate Std. Error  t value Pr(>|t|)    \n",
      "(Intercept) -3.264020   0.018838 -173.268  < 2e-16 ***\n",
      "areaA       -0.009818   0.028581   -0.344  0.73120    \n",
      "areaB       -0.005001   0.029586   -0.169  0.86578    \n",
      "areaD       -0.090445   0.037021   -2.443  0.01457 *  \n",
      "areaE       -0.115772   0.040674   -2.846  0.00442 ** \n",
      "areaF       -0.142623   0.046313   -3.080  0.00207 ** \n",
      "---\n",
      "Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1\n",
      "\n",
      "------------------------------------------------------------------\n",
      "Nu link function:  adjusted_exposure car$exposure \n",
      "Nu Coefficients:\n",
      "                       Estimate Std. Error t value Pr(>|t|)    \n",
      "(Intercept)            -1.74963    0.04856 -36.027  < 2e-16 ***\n",
      "agecat1                 0.28764    0.06225   4.621 3.83e-06 ***\n",
      "agecat2                 0.06435    0.04996   1.288 0.197780    \n",
      "agecat4                -0.03599    0.04756  -0.757 0.449181    \n",
      "agecat5                -0.26499    0.05553  -4.772 1.83e-06 ***\n",
      "agecat6                -0.25499    0.06682  -3.816 0.000136 ***\n",
      "areaA                  -0.03580    0.04509  -0.794 0.427158    \n",
      "areaB                   0.05338    0.04682   1.140 0.254302    \n",
      "areaD                  -0.13814    0.05827  -2.371 0.017764 *  \n",
      "areaE                  -0.06636    0.06472  -1.025 0.305198    \n",
      "areaF                   0.02086    0.07607   0.274 0.783918    \n",
      "veh_bodyBUS             1.13610    0.45568   2.493 0.012663 *  \n",
      "veh_bodyCONVT          -0.37088    0.63988  -0.580 0.562178    \n",
      "veh_bodyCOUPE           0.43332    0.14742   2.939 0.003291 ** \n",
      "veh_bodyHBACK          -0.01240    0.04298  -0.289 0.772905    \n",
      "veh_bodyHDTOP           0.09898    0.10457   0.946 0.343900    \n",
      "veh_bodyMCARA           0.59604    0.32352   1.842 0.065423 .  \n",
      "veh_bodyMIBUS          -0.11119    0.17171  -0.648 0.517276    \n",
      "veh_bodyPANVN           0.01942    0.14432   0.135 0.892980    \n",
      "veh_bodyRDSTR           0.06960    0.81606   0.085 0.932031    \n",
      "veh_bodySTNWG          -0.01913    0.04986  -0.384 0.701280    \n",
      "veh_bodyTRUCK          -0.09668    0.10806  -0.895 0.370950    \n",
      "veh_bodyUTE            -0.24555    0.07586  -3.237 0.001209 ** \n",
      "bands_veh_value25-50    0.21017    0.04927   4.266 2.00e-05 ***\n",
      "bands_veh_value25-75    0.13652    0.12368   1.104 0.269669    \n",
      "bands_veh_value75-100  -0.60666    0.54111  -1.121 0.262235    \n",
      "bands_veh_value100-125 -0.29002    0.77143  -0.376 0.706958    \n",
      "bands_veh_value>125    -0.79717    1.06226  -0.750 0.452990    \n",
      "---\n",
      "Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1\n",
      "\n",
      "------------------------------------------------------------------\n",
      "No. of observations in the fit:  67856 \n",
      "Degrees of Freedom for the fit:  46\n",
      "      Residual Deg. of Freedom:  67810 \n",
      "                      at cycle:  3 \n",
      " \n",
      "Global Deviance:     109609.7 \n",
      "            AIC:     109701.7 \n",
      "            SBC:     110121.5 \n",
      "******************************************************************\n"
     ]
    }
   ],
   "source": [
    "# Fit model with own nu.link\n",
    "fit = gamlss(\n",
    "  claimcst0 ~ agecat + area + gender,\n",
    "  sigma.formula=~ area,\n",
    "  nu.formula=~ agecat + area + veh_body + bands_veh_value,\n",
    "  family=ZAIG(nu.link=adjusted_exposure(exposure=car$exposure)),\n",
    "  data=car\n",
    ")\n",
    "\n",
    "summary(fit)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Bibliography\n",
    "\n",
    "1. De Jong, Piet, and Gillian Z. Heller. *Generalized linear models for insurance data*. Vol. 10. Cambridge: Cambridge University Press, 2008."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.3.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
